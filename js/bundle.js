(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){

'use strict';

var jQuery = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

/**
 *
 * Wrap jQuery
 *
 */
;(function ($) {

	/**
  *
  * Document Ready
  *
  */
	/* Outputs it is currently within a then, and allows to throw */
	/* a new error easily right after */

	function handleThen(id) {
		console.log('then ' + id);
		$.ajax({
			url: 'https://randomuser.me/api/?results=' + id,
			dataType: 'json',
			success: function success(data) {
				console.log(data.results);
			}
		});
		/* Returns an object with throwError function */
		/* to be able to throw a new error in a then */
		return {
			throwError: function throwError() {
				throw new Error('error ' + id);
			},
			reject: function reject() {
				return Promise.reject(new Error('error ' + id));
			}
		};
	}

	/* Outputs it is currently within a catch, and allows to throw */
	/* a new error easily right after */
	function handleCatch(id, error) {
		console.log('catch ' + id + ' : ' + error);
		/* Returns an object with rethrow function */
		/* to be able to rethrow the received error */
		/* in a catch */
		return {
			rethrow: function rethrow() {
				throw error;
			},
			reject: function reject() {
				return Promise.reject(error);
			}
		};
	}

	Promise.resolve().then(function () {
		return handleThen(1);
	})['catch'](function (e) {
		return handleCatch(1, e);
	}).then(function () {
		return handleThen(2);
	})['catch'](function (e) {
		return handleCatch(2, e);
	}).then(function () {
		return handleThen(3);
	})['catch'](function (e) {
		return handleCatch(3, e);
	}).then(function () {
		return handleThen(4);
	})['catch'](function (e) {
		return handleCatch(4, e);
	})['catch'](function (e) {
		return console.log('final catch : ' + e);
	}).then(function () {
		return console.log('Done');
	});
})(jQuery);

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}]},{},[1]);
