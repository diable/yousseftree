'use strict';



var gulp      = require('gulp'),
  plugins     = require('gulp-load-plugins')(),
  babelify        = require('babelify'),
  browserify      = require('browserify'),
  source          = require('vinyl-source-stream')
;



// Error Messaging
var onError = function(err) {

  plugins.notify.onError({
    title:    'Gulp Error',
    message:  '<%= error.message %>',
  })(err);

  this.emit('end');

};



// Lint Javascript
gulp.task('lint:scripts', function() {

  gulp.src(['src/**/*.js'])
    .pipe(plugins.eslint())
    .pipe(plugins.eslint.format())
    .pipe(plugins.eslint.failOnError())
    .on('error', onError);

});



// Compile Scripts
gulp.task('compile:scripts', function() {

  browserify({
    entries: ['src/scripts/index.js'],
    extensions: ['.js'],
    debug: false
  })
  .transform(babelify)
  .bundle()
  .on('error', onError)
  .pipe(source('bundle.js'))
  .pipe(gulp.dest('js'));

});



// Lint SCSS
gulp.task('lint:styles', function() {

  //gulp.src([
  //  'src/sass/base/**/*.scss',
  //  'src/sass/components/**/*.scss',
  //  'src/sass/pages/**/*.scss',
  //  'src/sass/master.scss'
  //  ])
  //  .pipe(plugins.scssLint({
  //    config: 'lint.yml'
  //  }));

});



// Compile Styles
gulp.task('compile:styles', function() {

  gulp.src('src/styles/style.scss')
    .pipe(plugins.cssGlobbing({
      extensions: ['.scss']
    }))
    .pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass({
      outputStyle: 'expanded'
    }))
    .on('error', onError)
    .pipe(plugins.autoprefixer())
    .pipe(plugins.sourcemaps.write('./', {
      sourceRoot: 'src/sass',
      includeContent: true
    }))
    .pipe(plugins.order([
      'css/**/*.css'
    ]))
    .pipe(gulp.dest('css'))
    .pipe(plugins.livereload());

});

// Watch package file for updates
gulp.task('npm:update', function() {

  var update = plugins.update();

  update.write({
    path: 'package.json'
  });

  gulp.watch('package.json').on('change', function(file) {

    if (file.type =='changed') {

      update.write({
        path: 'package.json'
      });

    }

  });

});



// Setup livereload
gulp.task('livereload:cleanup', plugins.shell.task([
  'LRPID=`lsof -n -i4TCP:35729 | grep LISTEN | awk \'{print $2}\'`' + "\n" +
  'if [ $LRPID ]' + "\n" +
  ' then' + "\n" +
  ' kill -9 $LRPID' + "\n" +
  'fi' + "\n"
]));



// Setup livereload
gulp.task('livereload:start', function() {

  plugins.livereload.listen();

});



// Watches for changes
gulp.task('watch', function() {

  gulp.watch('src/styles/**/*.scss', ['compile:styles']);
  gulp.watch('src/styles/**/*.scss', ['compile:IE9']);
  gulp.watch('src/scripts/**/*.js', ['lint:scripts', 'compile:scripts']);

  gulp.watch(['src/**/*.*', './**/*.php']).on('change', plugins.livereload.changed);

});


// Initialization
gulp.task('default', ['livereload:start', 'lint:scripts', 'lint:styles', 'compile:scripts', 'compile:styles', 'watch']);